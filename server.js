var  express = require('express')  
const app = express();
var expressGraphQL= require('express-graphql')
var schema = require('./schema/schema')



app.use('/graphql', expressGraphQL({
  schema,
  graphiql: true
}))

app.listen(4000, () => {
  console.log('Listening');
});
